import React from 'react'
import {
  View,
  Text
} from 'react-native'

const App = () => {
  return (
    <View>
      <Text>Welcome to CantooCanvas</Text>
    </View>
  )
}

export default App
